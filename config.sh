#! /bin/bash


#Downloading Miniconda 64Bits for Linux
wget https://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh

#Changing file permission for execution
chmod u+x Miniconda3-latest-Linux-x86_64.sh

#Installing Miniconda
./Miniconda3-latest-Linux-x86_64.sh

conda env create -f environment.yml
conda activate conda-crepo

which node | xargs -I {} ln -s {} {}js
pip install couchapp
npm install grunt grunt-contrib-watch grunt-couch

