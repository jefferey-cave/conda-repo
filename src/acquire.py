from cloudant.client import CouchDB
import couchdb
import datetime
import json
import progressbar
import re
import sys
import time
import urllib.request

CHANNELS = 1
DOWNLOADS = 100
CYCLES = 1000


def AcquireRepo(job):
	uri = job['value']['upstream']
	uri = '/'.join([uri,'repodata.json'])
	print('GET: ' + uri)
	try:
		contents = urllib.request.urlopen(uri).read()
		contents = contents.decode("utf-8")
	except Exception as e:
		print(' - ' + str(e))
		contents = '{}'
	contents = json.loads(contents)
	return contents


def SafePop(l):
	rtn = None
	if(len(l) > 0):
		rtn = l.pop()
	return rtn


def CreatePkgId(platform,fname):
	id = '/'.join(['pkg',platform,fname])
	return id


def CreateNewPackage(repocur,channel,subdir,uri):
	doc = {
		"_id": CreatePkgId(subdir,repocur[0]),
		"repodata":repocur[1],
		#"channel": channel,
	}
	doc['repodata']['channel'] = channel
	doc['repodata']['subdir'] = subdir

	doc['upstream'] = '/'.join([uri,repocur[0]])

	return doc


def SortSanitize(value):
	value = value.lower()
	#value = re.sub(r"[^0-9a-z]","-",value)
	value = value.encode("ascii","ignore")
	return value


def RepoDataPop(repodata,chName,subName):
	repocur = SafePop(repodata)
	repocur_id = ''
	repocur_id_sanitized = ''
	if(repocur):
		repocur_id = CreatePkgId(subName,repocur[0])
		repocur_id_sanitized = SortSanitize(repocur[0])
	return repocur_id, repocur, repocur_id_sanitized


def upsertRepoData(db,newdoc):
	id = newdoc['_id']
	logitems = {
		'000':"?? {0}  \n",
		'201':"++ {0}  \n",
		'202':"-+ {0}  \n",
		'303':"-- {0}  \n",
		'409':"== {0}  \r", 
		'417':"|| {0}  \r" 
	}
	handler = '.'.join(['_design/ch',newdoc['repodata']['channel']])
	try:
		resp = db.update_handler_result(handler, 'packages', id, data=json.dumps(newdoc))
	except Exception as e:
		print('ERROR updating document: ' + id)
		print(e)
		raise e

	resp = json.loads(resp)
	try:
		resp = resp['status']
		logitem = logitems[str(resp)]
	except:
		logitem = logitems['000']
	logitem = logitem.format(id)
	print(logitem,end='\r')


def AcquireRepoChannelList(db,job):
	chName = job['key'][2].split('/')
	subName = chName[1]
	chName = chName[0]
	repopath = job['value']['upstream']

	packdata = db.get_view_result('_design/packages','packages',raw_result=True,descending=True,stable=False,update="true",include_docs=True,endkey=[chName,subName],startkey=[chName,subName,{}])
	packdata = packdata['rows']
	packdata = sorted(packdata,key=lambda d: SortSanitize(d['id']),reverse=True)
	#packdata = []

	repodata = AcquireRepo(job)
	repodata = list(repodata['packages'].items())
	repodata = sorted(repodata,key=lambda d: SortSanitize(d[0]),reverse=True)
	#repodata = []

	repocur_id, repocur, repocur_id_sanitized = RepoDataPop(repodata,chName,subName)
	changes = []
	while (len(packdata) > 0):
		pkg = packdata.pop()
		id = pkg['id']
		id_sanitized = SortSanitize(pkg['key'][2])
		while(repocur_id_sanitized <= id_sanitized):
			doc = CreateNewPackage(repocur,chName,subName,repopath)
			upsertRepoData(db,doc)
			repocur_id, repocur, repocur_id_sanitized = RepoDataPop(repodata,chName,subName)
		
		if(repocur_id_sanitized > id_sanitized):
			doc = pkg['doc']
			doc['_deleted'] = True
			upsertRepoData(db,doc)

	while(repocur):
		doc = CreateNewPackage(repocur,chName,subName,repopath)
		upsertRepoData(db,doc)
		repocur_id, repocur, repocur_id_sanitized = RepoDataPop(repodata,chName,subName)
	
	# update the channel last check date
	now = datetime.datetime.utcnow().isoformat()
	print("\nChannel completed: {0}\n\n".format(now))
	id = '.'.join(['_design/ch',chName])
	doc = db[id]
	doc['settings']['upstream']['checked'][subName] = now
	doc.save()


def AcquirePackages(db,job):
	id = job['id']
	url = job['value']['upstream']
	try:
		data = urllib.request.urlopen(url).read()
	except Exception as e:
		print('\n - ' + str(e))
		contents = '{}'
	try:
		doc = db[id]
		doc.put_attachment(job['value']['hash'], 'application/bzip2', data)
	except Exception as e:
		print('\n - ' + str(e))

	print('\r+  ' + url)


def FindJobs(db):
	jobs = []
	size = (1024**3)
	endkey = datetime.datetime.utcnow().isoformat()
	candidates = db.get_view_result('_design/packages','jobs',
		limit=DOWNLOADS,
		raw_result=True,
		startkey=[],
		endkey=[endkey]
	)
	for candidate in candidates['rows']:
		if(size < 0):
			break
		resp = db.update_handler_result('_design/packages', 'lock', candidate['id'])
		if(resp == 'HTTP: 401 locked'):
			continue
		size -= candidate['value']['cost']
		jobs.append(candidate)

	return jobs


def ProcessData(uri):
	uri = uri.split('/')
	handlers = {
		"discover": AcquireRepoChannelList,
		"attach": AcquirePackages
	}

	dbname = ''
	while(dbname == ''):
		dbname = uri.pop()
	server = '/'.join(uri)
	uri = '/'.join(uri)

	client = CouchDB('jeff','hellojello',url=server,connect=True,auto_renew=True)
	try:
		db = client[dbname]
		delay = 60

		print("\n\n - {0}\n - {1}\n".format(server,dbname))
		for i in range(0,CYCLES):
			now = datetime.datetime.utcnow().isoformat()
			print(" - {0}".format(now.split('.')[0]))
			for job in FindJobs(db):
				handler = job['value']["job"]
				handler = handlers[handler]
				try:
					handler(db,job)
				except Exception as e:
					print("job failure")
				delay = 1.6
			time.sleep(delay)
			delay = min(delay*1.618, 3600)

	finally:
		client.disconnect()


if __name__ == "__main__":
	uri = 'http://lvh.me:5984/conda/'
	if(len(sys.argv) > 1):
		uri = sys.argv[1]
	print('Syncing: '+uri)
	ProcessData(uri)
