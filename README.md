# conda-couchrepo

Maintaining a conda repository for internal users can be hard, this tool is 
designed to ease some of the pain of that maintenance.

Specifically, this repository mirroring tool is designed to work in 
environments where the connection to the outside world is ... flakey.

* security air-gapped environments
* remote helicopter-gapped environments
* metered satelite-gapped environments

The idea is that it is cheaper, faster, more reliable to access a repository 
hosted on the computer next to you, than it is to 

Currently, other solutions to this problem do a fine job, but assume you have 
a reliable connection at some point in the chain, and that you can trust the 
job to finish.

That just isn't the case.

CouchRepos overcomes this problem two ways:
1. CouchRepo works slowly... but surely... to completely download and index 
   each package it synchronizes. This ensures that in the event of a failure, 
   at least part of the repository updates are available to you.
2. It uses 
   <abbr title='cluster of unreliable commodity hardware'>CouchDB</abbr> 
   to store, and more importantly, synchronize your repository. This allows 
   you to take advantage of CouchDB's strong syncronization capabilities to 
   sync up with other CouchRepos.


