(function(){

'use strict';

module.exports = function(grunt) {
	//const fs = require('fs');

	grunt.initConfig({});

	grunt.config.merge({
		target: (function(){
			var targ = '';
			try {
				targ = JSON.parse(process.env.deployment);
			}
			catch (e) {
				targ = {
					port: process.env.PORT || 5984,
					hostname: 'lvh.me',
					db:'conda',
					protocol:'http',
				};
			}
			targ.url = targ.protocol + '://' + targ.hostname + ':' + targ.port +'/'+targ.db+'/';
			return targ;
		})(),
		isProd : (process.env.isProd == 'true')
	});

	grunt.config.merge({
		watch: {
			files: ['couchapp/**/*'],
			tasks: ['build']
		},
		'couch-compile': {
			app: {
				files: {
					'bin/data_cubes.json': 'couchapp/data_cubes',
					'bin/interface.json': 'couchapp/interface',
					'bin/packages.json': 'couchapp/packages',
					'bin/ch.conda-forge.json': 'couchapp/ch.conda-forge',
					'bin/ch.free.json': 'couchapp/ch.free'
				}
			}
		},
		'couch-push': {
			app: {
				options: {
					user: 'jeff',
					pass: 'hellojello'
				},
				files: (()=>{
					var f = {};
					//grunt.log.write("HERE:"+grunt.config.get('target').url);
					f[grunt.config.get('target').url] = [
						'bin/data_cubes.json',
						'bin/interface.json',
						'bin/packages.json',
						'bin/ch.conda-forge.json',
						'bin/ch.free.json'
					];
					return f;
				})()
			}
		},
	});


	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-couch');

	grunt.registerTask('default', ['build']);
	grunt.registerTask('build', ['deploy']);
	grunt.registerTask('deploy', [
		'couch-compile',
		'secretkeys',
		'couch-push'
	]);

	grunt.task.registerTask('secretkeys', 'Replace various keys', function() {
		var secrets;
		//grunt.log.write('HERE:'+ JSON.stringify(JSON.parse(process.env.oauthKeys),null,4) + '\n');
		try{
			secrets = JSON.parse(process.env.secrets);
		}
		catch(e){
			secrets = {patreon:{}};
		}
		var replaces = {
			'==secrets.patreon.TokenFromUs==':secrets.patreon.TokenFromUs || '==secrets.patreon.TokenFromUs==',
			'==secrets.patreon.TokenFromThem==':secrets.patreon.TokenFromThem || '==secrets.patreon.TokenFromThem==',
		};
		const child = require('child_process');
		grunt.file.expand('bin/**/*.json').forEach(function(file) {
			grunt.log.write(`${file} \n`);
			for(let key in replaces){
				grunt.log.write(` - ${key} \n`);
				let orig = key.replace(/~/g,'\\~');
				let n = replaces[key];
				let cmd = 'sed -i s~{{orig}}~{{new}}~g {{file}}'
					.replace(/{{file}}/g,file)
					.replace(/{{orig}}/g,orig)
					.replace(/{{new}}/g,n.replace(/~/g,'\\~'))
					;
				//grunt.log.write(` ${cmd} \n`);
				child.execSync(cmd);
			}
		});
	});

};


})();
