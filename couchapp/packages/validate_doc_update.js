function(newDoc, oldDoc, userCtx){
	var indexSep = '/';
	var isChanged = (function(oldDoc,newDoc){
			if(!oldDoc) return true;

			var o = JSON.parse(JSON.stringify(oldDoc));
			var n = JSON.parse(JSON.stringify(newDoc));
			o._rev = "";
			o._revisions = "";
			n._rev = "";
			n._revisions = "";
			o = JSON.stringify(o);
			n = JSON.stringify(n);

			return (n !== o);
		})(oldDoc,newDoc);
	if(!isChanged){
		throw({forbidden : "Entity is unchanged. Nothing to do."});
	}

	//var type = newDoc._id.split(indexSep).shift()
	//if(type === 'pkg'){
	//	if(!newDoc.channels){
	//		throw({forbidden : 'Packages must note what Channels they are associated with.'});
	//	}
	//	if(!newDoc.subdir){
	//		throw({forbidden : 'Packages must note what subdir they are associated with.'});
	//	}
	//}
}
