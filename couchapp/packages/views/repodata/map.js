function(doc){
	if(!/^pkg\//.test(doc._id)) return;
	if(!doc._attachments) return;

	var util = require('views/lib/utils');

	var id = doc._id.split('/').pop();

	for(var channel in doc.channels){
		var hash = doc.channels[channel];
		var repodata = doc.packages[hash];
		var attachment = doc._attachments[hash];
		if(!attachment) return;

		var md5 = repodata.md5;
		md5 = util.htob(md5);
		md5 = 'md5-' + util.btoa(md5);
		if(attachment.digest !== md5) return;

		var key = [channel, repodata.subdir, id];
		emit(key,repodata);
	}
}
