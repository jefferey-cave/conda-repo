function (doc){
	const JobTypes = {
		discover:0,
		attach:1,
	}
	const indexSep = '/';
	var lock = '1977-12-20';
	if(doc && doc.lock && doc.lock.until){
		lock = doc.lock.until
	}
	lock = new Date(lock).toISOString();
	function send(key,job){
		var timer = [lock,key.shift()];
		timer = timer.sort();
		job.at = timer.pop();
		key.unshift(job.at);
		emit(key,job);
	}

	if(/^pkg\//.test(doc._id)){

		for(var hash in doc.packages){
			if(doc._attachements && doc._attachements[hash]){
				continue;
			}
			var repodata = doc.packages[hash];
		  
			var nextcheck = Date.parse('1982-12-20');
			// The quickest way I could think to randomize the order 
			// was to convert the MD5 to the time of day portion.
			nextcheck += (parseInt(repodata.md5,16)%86400000);
			nextcheck = new Date(nextcheck).toISOString();

			var key = [nextcheck,1,repodata.md5];
			var job = {
				job: 'attach',
				upstream: doc.upstream,
				downstream: doc._id,
				lock: doc.lock,
				cost: repodata.size,
				hash: hash,
			};
			send(key,job);
		}
	}
	else if(/^_design\/ch\./.test(doc._id)){

		const HOUR = 3600000;
		for(var s in doc.settings.subdirs){
			var subdir = doc.settings.subdirs[s];

			var nextcheck = doc.settings.upstream.checked[subdir] || '1977-12-20';
			nextcheck = Date.parse(nextcheck);
			// Every 4 hours
			nextcheck += (HOUR*8);
			// +/- a half hour to avoid any significant server overload or patterns.
			nextcheck += Math.floor((Math.random()-0.5)*HOUR);
			nextcheck = new Date(nextcheck).toISOString();

			key = [nextcheck,0,[doc.settings.name,subdir].join('/')];
			job = {
				job: 'discover',
				upstream: [doc.settings.upstream.uri,subdir].join('/'),
				downstream: [doc._id,'_updates','packages'].join('/'),
				lock: doc.lock,
				cost: Math.pow(1024,4),
			};
			send(key,job);
		}
	}
}
