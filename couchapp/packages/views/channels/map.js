function(doc){
	if(!/^_design\/ch\./.test(doc._id)) return;

	const HOUR = 3600000;
	for(var s in doc.settings.subdirs){
		var subdir = doc.settings.subdirs[s];

		var nextcheck = doc.settings.upstream.checked[subdir] || '1977-12-20';
		nextcheck = nextcheck.replace(/[^0-9]/g,'');
		nextcheck = (nextcheck + '00000000000000');
		nextcheck = nextcheck.substr(0,14);
		nextcheck =
			nextcheck.substr(0,4) + '-' +
			nextcheck.substr(4,2) + '-' +
			nextcheck.substr(6,2) + 'T' +
			nextcheck.substr(8,2) + ':' +
			nextcheck.substr(10,2) + ':' +
			nextcheck.substr(12,2)
			;
		nextcheck = Date.parse(nextcheck);
		// Every 4 hours
		nextcheck += (HOUR*4*168);
		// +/- a half hour to avoid any significant server overload or patterns.
		nextcheck += Math.floor((Math.random()-0.5)*HOUR);
		nextcheck = new Date(nextcheck)
			.toISOString()
			.split('.')[0]
			.replace(/[^0-9]/g,'')
			;

		var key = [nextcheck,doc.settings.name,subdir];
		emit(key, doc.settings.upstream.uri);
	}
}
