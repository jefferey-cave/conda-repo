function(doc){
	if(!/^pkg\//.test(doc._id)) return;

	var indexSep = '/';

	var id = doc._id.split(indexSep);
	id.shift();
	id.shift();
	id.shift();
	id = id.join(indexSep);
	id = id.toLowerCase();

	for(var channel in doc.channels){
		var sha = doc.channels[channel];
		var repodata = doc.packages[sha];
		var key = [
			channel,
			repodata.subdir,
			id
		];
		emit(key,sha);
	}
}