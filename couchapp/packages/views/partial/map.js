function(doc){
	if(!/^pkg\//.test(doc._id)) return;

	var util = require('views/lib/utils');

	for(var channel in doc.channels){
		var hash = doc.channels[channel];
		var repodata = doc.packages[hash];
		var md5 = repodata.md5;
		md5 = util.htob(md5);
		md5 = 'md5-' + util.btoa(md5);
		if(doc._attachments && doc._attachments[hash] && doc._attachments.blob.digest === md5) return;

		var key = [
			repodata.md5
		];
		emit(key,doc.upstream);
	}
}
