function(head, req){
	/**
	 * There are a few types we handle
	 */
	registerType('json', 'application/json');
	registerType('html', 'text/html');
	registerType('bzip', 'application/bzip2');


	/**
	 * JSON provider
	 */
	provides('json',function(){
		start({
			'headers': {
				'Content-Type': 'text/json'
			}
		});
		send('{');
		send('  "repodata_version": 1,');
		send('	"packages":{');
		var subdir = '';
		var sep = '';
		var row = null;
		while(row = getRow()){
			var name = row.id.split('/').pop();
			send(sep+JSON.stringify(name) + ':');
			send(JSON.stringify(row.value));
			//send(JSON.stringify({}));
			subdir = row.value.subdir||subdir;
			sep = ',';
		}
		send('	},');
		send('  "info": { "subdir": "'+subdir+'" } ');
		send('}');
	});


	/**
	 * BZip provider
	 */
	provides('bzip',function(){
		start({
			'headers': {
				'Content-Type': 'application/bzip2'
			}
		});
		var repodata = {};
		repodata.repodata_version = 1;
		repodata.packages = {};
		var subdir = '';
		var sep = '';
		var row = null;
		while(row = getRow()){
			var name = row.id.split('/').pop();
			repodata.packages[name] = row.value;
			subdir = row.value.subdir||subdir;
		}
		repodata.info = {
			subdir: subdir
		};
		repodata = JSON.stringify(repodata);
		var compressjs = require('views/lib/compressjs/main');
		repodata = compressjs.Bzip2.compressFile(repodata).join('');
		return repodata;
	});


	/**
	 * HTML provider
	 */
	provides('html',function(){
		start({
			'headers': {
				'Content-Type': 'text/html'
			}
		});
		send('<html>');
		send('<head>');
		send(' <title>Conda packages for ???</title>');
		send(' <link rel="stylesheet" type="text/css" href="../../theme.css">');
		send('</head>');
		send('<body>');
		send(' <h2>Conda packages for ???</h2>');
		send(' <table>');
		send('  <tr>');
		send('   <th>Filename</th>');
		send('   <th>Size</th>');
		send('   <th>Last Modified</th>');
		send('   <th>MD5</th>');
		send('  </tr>');
		var row = null;
		var count = 0;
		var lastmodified = 0;
		while(row = getRow()){
			count++;
			var name = row.id.split('/').pop();
			subdir = row.value.subdir||subdir;
			var timestamp = row.value.timestamp||0;
			lastmodified = Math.max(lastmodified,timestamp);
			timestamp = timestamp===0?'':new Date(timestamp).toISOString().split('.')[0].replace('T',' ');
			send('  <tr>');
			send('   <td><a href="{{name}}">{{name}}</a></td>'.replace(/{{name}}/g,name));
			send('   <td class="s">'+HumanizeSize(row.value.size)+'</td>');
			send('   <td>'+timestamp+'</td>');
			send('   <td>'+row.value.md5+'</td>');
			send('  </tr>');
		}
		lastmodified = new Date(lastmodified).toISOString().split('.')[0].replace('T',' ');
		send(' </table>');
		send(' <address>Updated: '+lastmodified+'Z - Files: '+count.toFixed(0)+'</address>');
		send('</body>');
		send('</html>');
	});

	/**
	 * Utility functions
	 * 
	 * @param {int} bytes The number of bytes to convert
	 * @returns {string} A textual representation of the length
	 */
	function HumanizeSize(bytes){
		var abbrs = ['','KB','MB','GB','TB','PB'];
		for(var abbr = abbrs.shift(); abbrs.length > 0 && bytes > 1000; abbr = abbrs.shift()){
			bytes /= 1024;
		}
		bytes = bytes.toFixed(0);
		return [bytes,abbr].join(' ');
	}
}
