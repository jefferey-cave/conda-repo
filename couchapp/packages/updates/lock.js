function(doc,req){
	if(doc){
		var now = new Date();
		if(doc.lock){
			var until = Date.parse(doc.lock.until);
			if(until > now){
				return [null,"HTTP: 401 locked"];
			}
		}
		doc.lock = {
			name: 'me',
			until: new Date()
		}

		doc.lock.until.setHours(doc.lock.until.getHours()+1);
		//doc.lock.until.setMinutes(-1 * doc.lock.until.getTimeZoneOffset());
		doc.lock.until = doc.lock.until.toISOString();
		return [doc,"HTTP: 200 - OK"];
	}
	return [null,"HTTP: 404 - Not Found"];
}
