function (doc) {
	var filter = require('views/lib/packages').filter;
	filter = filter(doc);
	emit(doc._id, JSON.stringify(filter));
}
