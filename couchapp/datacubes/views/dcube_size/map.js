function(doc){
	if(!/^pkg\//.test(doc._id)) return;


	var status = 'pending';
	if(doc._attachments){
		status = 'complete';
	}

	var key_labels = ['channel','platform','status'];
	var key_values = [
		doc.repodata.channel,
		doc.repodata.subdir,
		status
	];
	var combos = [
	    [0,1,2],
	    [0,2,1],
	    [1,0,2],
	    [1,2,0],
	    [2,0,1],
	    [2,1,0],
	  ];
	for(var combo in combos){
		combo = combos[combo];
		var label = [];
		var key = [];
		for(var c in combo){
			c = combo[c];
			label.push(key_labels[c]);
			key.push(key_values[c]);
		}
		label = label.join('.');
		key.unshift(label);
		emit(key,doc.repodata.size);
	}

}