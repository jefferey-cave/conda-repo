function filter(repodata,settings){
	var isallowed = true;
	for(var i=0; i<settings.blacklist.length && isallowed; i++){
		var rules = settings.blacklist[i];
		for(var r in rules){
			var rule = rules[r];
			rule = "^" + rule + "$";
			rule = new RegExp(rule);
			if(rule.test(repodata[r])){
				isallowed = false;
				continue;
			}
		}
	}

	for(var i=0; i<settings.whitelist.length && !isallowed; i++){
		var rules = settings.whitelist[i];
		for(var r in rules){
			var rule = rules[r];
			rule = "^" + rule + "$";
			rule = new RegExp(rule);
			if(rule.test(repodata[r])){
				isallowed = true;
				continue;
			}
		}
	}

	return isallowed;
}

function isChanged(oldDoc,newDoc){
	if(!oldDoc) return true;

	var o = JSON.parse(JSON.stringify(oldDoc || {}));
	var n = JSON.parse(JSON.stringify(newDoc || {}));
	for(var key in o){
		if(/^_/.test(key)){
			delete o[key];
		}
	}
	for(var key in n){
		if(/^_/.test(key)){
			delete n[key];
		}
	}
	delete o.meta;
	delete n.meta;
	o = JSON.stringify(o);
	n = JSON.stringify(n);

	return (n !== o);
};

exports['filter'] = filter;
exports['isChanged'] = isChanged;
