function(doc,req){
	var pkgUtils = require('views/lib/packages');
	var hashes = require('views/lib/utils');

	/**
	 * 
	 * @param {*} doc 
	 */
	function InitializeDocument(doc,newDoc){
		// If this is a new object, create the initial object
		if(!doc){
			var id = newDoc._id.split('/').pop();
			doc = {
				"_id": newDoc._id,
				"upstream": [settings.upstream.uri,repodata.subdir,id].join('/'),
				"packages": {},
				"channels": {},
			};
		}
		return doc;
	}

	/**
	 * Removes any unused Package definitions from the listing.
	 * 
	 * @param {hashmap} packages 
	 * @param {hashmap} channels 
	 */
	function CleanPackages(packages,channels){
		// collect up al the package SHAs to 
		// find ones that need to be removed
		var pSha = {};
		for(var sha in packages){
			pSha[sha] = null;
		}
		// check every channel, if a channel is assigned to a package then we 
		// need to keep the package. Therefore remove the SHA from the kill 
		// list.
		for(var c in channels){
			var sha = channels[c]
			delete pSha[sha];
		}
		// Remove all the packages that are left behind. These are the ones 
		// that aren't associated iwth any channel.
		for(var sha in pSha){
			delete packages[sha];
		}
		// After having removed all the packages that we no longer use, we 
		// should check that the channels actually have a package associated 
		// with them. If not, turf them.
		var rem = [];
		for(var c in channels){
			var sha = channels[c];
			if(!(sha in packages)){
				rem.push(c);
			}
		}
		for(var c=rem.pop(); rem.length>0; c=rem.pop()){
			delete channels[c];
		}
	}

	/**
	 * Clean the attachments
	 * 
	 * if after updating the record, the MD5's are not the same, then we 
	 * should delete the attachment to mark it for fresh acquisition
	 * @param {*} doc 
	 * @param {*} sha 
	 */
	function CleanAttachments(doc,sha){
		if(doc._attachments && doc._attachments[sha]){
			// convert the MD5 on the package (a hex value) to 
			// Couch's (Base64 style)
			var couchstyle = hashes.hexToCouch(repodata.md5);
			if(doc._attachments[sha].md5 !== couchstyle){
				delete doc._attachments[sha];
			}
		}
	}

	var orig = '';
	var settings = this.settings;
	var package = req.body;
	package = JSON.parse(package);
	var repodata = package.repodata || {};
	var channel = this.settings.name;
	var msg = {'status':202,'reason':'Updated'};

	// not everything has a SHA, so we need to include
	// MD5's as a possibility
	var sha = repodata.sha256;
	if(sha){
		sha = ['sha256',sha].join('-');
	}
	else{
		sha = repodata.md5;
		sha = ['md5',sha].join('-');
	}

	doc = InitializeDocument(doc,package);
	// if this is a new package, make a note of it
	if(!doc.packages[sha]){
		msg.status = 201;
	}
	orig = JSON.parse(JSON.stringify(doc.packages[sha] || {}));

	// record the package and the channel;
	doc.packages[sha] = repodata;
	doc.channels[channel] = sha;

	// if a delete was issued, clear the channel
	if(package._deleted || req.method === 'DELETE'){
		delete doc.channels[channel];
		msg = {'status':303,'reason':'Deleted from channel'};
	}

	// validate that inbound package matches the filter requirements for this 
	// channel. In the case of removal, all we need to do is remove it from 
	// the channel
	delete repodata.barred;
	var filter = pkgUtils.filter;
	var valid = filter(doc.packages[sha],this.settings);
	if(!valid){
		repodata.barred = "Blocked for some reason or another.";
		delete doc.channels[channel];
		msg = {'status':417,'reason':'Does not meet criteria for inclusion in this channel.'};
	}


	/**** finalize status and overall package state ****/ 
	CleanPackages(doc.packages,doc.channels);
	CleanAttachments(doc,sha);
	if(doc.channels.length<0){
		doc._deleted = true;
	}

	// Vital statistics should be updated
	var now = (new Date()).toISOString();
	doc.meta = doc.meta || {
		created: now
	};
	doc.meta.updated = now;

	// I can't think of an elegant way to handle this case:
	// If the document has been deleted, but did not exist in the
	// first place, we probably should not create it at all.
	// Just reset the repodata object and let things take care 
	// of themselves
	if(doc._deleted && orig === '{}'){
		doc.packages[sha] = JSON.parse(orig);
	}
	// if (after all that) there are no significant changes, we should not 
	// bother saving the document. 
	if(!pkgUtils.isChanged(orig,doc.packages[sha])){
		msg = {'status':409,'reason':'No significant change'};
		doc = null;
	}

	// send the document
	return [doc,JSON.stringify(msg)];
}
