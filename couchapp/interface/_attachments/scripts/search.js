import {crepo} from './crepo.js'

function initialize(){
	let input = document.querySelector('[name="search"]')
	input.search = new Search(crepo.db,input);
}

window.addEventListener('load',()=>{
	initialize();
});

class Search{
	constructor(db, input){
		this.db = db;
		this.input = input;
		this.data = document.querySelector('datalist')

		this.input.addEventListener('input', async (e)=>{
			let text = e.target.value;
			if(text.length < 2){
				return;
			}
			let opts = {
				startkey: text,
				endkey: text + '\ufff0',
				limit:100,
				group_level:1,
				group:true,
				reduce:true,
			};
			this.data.innerHTML = '';
			let results = await db.query('interface/search',opts);
			results.rows.sort((a,b)=>{return b.value-a.value;});
			for(let r=0; r<5 && results.rows.length>0; r++){
				let row = results.rows.shift();
				let opt = document.createElement('option');
				opt.value = row.key;
				this.data.append(opt);
			}
		});
	}
}