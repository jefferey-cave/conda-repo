import {crepo} from './crepo.js';
import 'https://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.9.1/underscore-min.js';

class DownloadProgress{

	constructor(element,db,opts = {}){
		this._ = {
			db: db,
			filter: opts.filter || [],
			element:element,
			query:'interface/size',
		}
		
		let dataloader = null;
		if(opts.data){
			dataloader = Promise.resolve(opts.data);
		}
		else{
			dataloader = this.LoadFromDb().then((result)=>{
				return result[0];
			});
		}
		dataloader.then((data)=>{
			this.RenderUpdate(data);
		});

		let changeFilter = this._.filter.slice();
		changeFilter.unshift('pkg');
		changeFilter = changeFilter.join('/')
		let updater = async (change)=>{
			let result = await this.LoadFromDb()
			this.RenderUpdate(result[0]);
		};
		updater = _.throttle(updater,333);
		db.changes({live:true,since:'now',
			filter: (doc)=>{
				if(doc._id.startsWith(changeFilter)){
					return doc;
				}
			}})
			.on('change', updater)
			;
	}

	get Data(){
		return this._.data;
	}

	async LoadFromDb(level=this._.filter.length){
		let opts = {
			reduce:true,
			group_level:level
		}
		opts.startkey = this._.filter;
		opts.endkey = this._.filter.slice();
		opts.endkey.push({});
		let results = await this._.db.query(this._.query,opts);
		return results.rows;
	}

	RenderInitial(){
		let summary = this._.element.querySelector('summary');
		if(!summary){
			summary = this._.element;
		}
		this._.element.innerHTML = [
			'<summary>',
			' <span>',
			'  <output name="label">0</output>',
			'  <output name="complete_count">0</output>',
			'  <output name="total_count">0</output>',
			'  <output name="pct_count">0</output>',
			' </span>',
			' <progress name="size" value="0" min="0" max="1">',
			' </progress>',
			'</summary>'
		].join('');
		this._.summary = summary = this._.element.querySelector('summary');
		this._.element.removeAttribute('open');

		let firstExpand = ()=>{
			this._.element.removeEventListener('click',firstExpand);
			this.RenderChildren();
		};
		this._.element.addEventListener('click',firstExpand);
	}

	RenderUpdate(data){
		if(!this._.summary) this.RenderInitial();
		let elems = {};
		Array.from(this._.summary.querySelectorAll('[name]')).forEach(e=>{
			let name = e.getAttribute('name');
			elems[name] = e;
		});

		if(!data){ 
			elems.label.value = "No data.";
			return;
		}
		
		let key = (data.key || []).slice();
		data = data.value;

		elems.label.value = key.pop() || '';
		elems.size.value = data.complete.sum;
		elems.size.max = data.total.sum;
		elems.complete_count.value = data.complete.count;
		elems.total_count.value = data.total.count;
		elems.pct_count.value = Math.floor(data.complete.count / data.total.count * 100.0);
	}

	async RenderChildren(){
		const db = this._.db;
		const parent = this._.element;

		let loading = document.createElement('span')
		loading.textContent = "Loading....";
		parent.append(loading);

		let rows = await this.LoadFromDb(this._.filter.length+1);
		loading.remove();
		for(let r=0; r<rows.length; r++){
			let rec = rows[r];
			if(rec.key.length === this._.filter.length){
				this._.element.classList.add('leaf');
				break;
			}

			let child = document.createElement('details');
			parent.append(child);
			child = new DownloadProgress(child,db,{
				filter:rec.key,
				data:rec
			});
		}
	}
}

class Clock extends HTMLElement{
	constructor(){
		super();
		this._ = {};
		this.offset = 0;
	}

	connectedCallback(){
		if(!this._.ticker){
			this._.ticker = setInterval(()=>{ 
				this.update(); 
			},10000);
			this.update();
		}
	}

	disconnectedCallback(){
		if(this._.ticker){
			clearInterval(this._.ticker);
			this._.ticker = null;
		}
	}

	get now(){
		let time = Date.now();
		let offset = time - this.offset;
		let clamped = Math.max(0,offset);
		return clamped;
	}

	toString(){
		let time = this.now;
		if(time < 10){
			return "Promptly...";
		}

		time = new Date(time);
		time = time.toISOString();
		time = time.replace(/T/,'.').split('.');
		time = time[1].substr(0,5);
		return time;
	}

	update(){
		this.textContent = this.toString();
	}
}
customElements.define('crepo-clock', Clock);

class CountDown extends Clock{
	constructor(){
		super();
		this._ = {value:Date.now()};
	}

	get value(){
		return this._.value;
	}
	set value(val){
		val = new Date(val);
		val = val.getTime();
		if(val !== this._.value){
			this._.value = val;
			this.update();
		}
	}
	get target(){
		return this.value;
	}
	set target(value){
		this.value = value;
	}

	get now(){
		let time = super.now;
		let dist = this.target - time;
		return dist;
	}

	toString(){
		let str = super.toString();
		if(str.endsWith('...')){
			return str;
		}
		str = str.replace(':','h ');
		str = str + 'm';
		return str;
	}
}
customElements.define('crepo-countdown', CountDown);


class Jobs extends HTMLElement{
	constructor(){
		super();
		this._ = {};
		this.db = crepo.db;
		this.opts = {
			limit:5
		};

		this._.shadow = this.attachShadow({mode:'open'});
		this._.head = this.querySelector('header');
		if(!this._.head){
			this._.head = document.createElement('header');
		}
		this._.shadow.append(this._.head);
		this._.main = this.querySelector('main');
		if(!this._.main){
			this._.main = document.createElement('main');
			this._.main.innerHTML = this.innerHTML;
		}
		this._.shadow.append(this._.main);
		this._.template = this._.main.innerHTML;
		this._.headTemplate = this._.head.innerHTML;

		this.LoadFromDb()
			.then((data)=>{
				this.RenderUpdate(data);
			});

		let updater = async (change)=>{
			let result = await this.LoadFromDb()
			this.RenderUpdate(result);
		};
		updater = _.throttle(updater,3333);
		this.db.changes({live:true,since:'now'}).on('change', updater);
	}
	async LoadFromDb(){
		let results = await this.db.query('packages/jobs',this.opts);
		return results.rows;
	}
	RenderUpdate(rows){
		this._.head.innerHTML = '';
		this._.main.innerHTML = '';
		for(let row in rows){
			row = rows[row];
			let item = document.createElement('div');
			item.innerHTML = this._.template;
			let outputs = Array.from(item.querySelectorAll('[name]'));
			if(!this._.head.innerHTML){
				this._.head.innerHTML = this._.headTemplate;
				outputs = outputs.concat(Array.from(this._.head.querySelectorAll('[name]')));
			}
			for(let out in outputs){
				out = outputs[out];
				let name = out.getAttribute('name');
				if(name in row.value){
					out.value = row.value[name];
				}
			}
			this._.main.append(item);
		}
	}
}
customElements.define('crepo-jobs', Jobs);


window.addEventListener('load',()=>{
	let progress = document.querySelector('#downloadprogress');
	progress = new DownloadProgress(progress,crepo.db);
});
