function(doc){
	if(!/^pkg\//.test(doc._id)) return;
	if(!doc._attachments) return;

	var indexSep = '/';
  
	var text = JSON
		.stringify(doc)
		.toLowerCase()
		.replace(/[^a-z]/g,' ')
		.split(' ')
		;

	for(var w in text){
		for(var word = text[w].split(''); word.length>3; word.shift()){
			var key = word.join('');
			emit(key,1);
		}
	}
}