function(doc){
	if(!/^pkg\//.test(doc._id)) return;
	if(!doc._attachments) return;

	for(var channel in doc.channels){
		var hash = doc.channels[channel];
		var repodata = doc.packages[hash];
		var attachment = doc._attachments[hash];
		if(!attachment) return;

		var key = repodata.name;
		emit(key,1);
	}
}
