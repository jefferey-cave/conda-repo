function(doc){
	if(!/^pkg\//.test(doc._id)) return;

  var id = doc._id.split('/').pop();

	for(var channel in doc.channels){
		var hash = doc.channels[channel];
		var repodata = doc.packages[hash];
		var attach = doc._attachments;

		var status = 'pending';
		if(attach && attach[hash]){
			status = 'complete';
		}

		var key = [channel,repodata.subdir,id,hash,status];
		var val = {};
		val[status] = repodata.size;
		emit(key,val);
	}
}