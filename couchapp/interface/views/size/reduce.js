function(keys, values, rereduce) {
	function create(d){
		if(typeof d === 'object'){
			return d;
		}
		return{
			'sum':d,
			'min':d,
			'max':d,
			'count':1
		};
	}
	var empty = {sum:0,min:Infinity,max:-Infinity,count:0};
	function add(a, b) {
		a = create(a || empty);
		b = create(b || empty);
		return {
			'sum': a.sum + b.sum,
			'min': Math.min(a.min, b.min),
			'max': Math.max(a.max, b.max),
			'count': a.count + b.count 
		};
	}

	var result = {};

	result.pending = values.reduce(function(a,d){return add(a,d.pending);},empty);
	result.pending.avg = Math.floor((result.pending.sum / result.pending.count) || 0);

	result.complete = values.reduce(function(a,d){return add(a,d.complete);},empty);
	result.complete.avg = Math.floor((result.complete.sum / result.complete.count) || 0);

	result.total = add(result.pending,result.complete);
	result.total.avg = Math.floor((result.total.sum / result.total.count) || 0);

	return result;
}
